# Pocket Pi Terminal

Home for the design and planning for my pocket rasperry pi terminal project.

This project is ongoing, janky, and in no way the best way to do this I am sure.

I am by no means an expert, I am just a student looking into a cool idea I had.
 
The general idea for this is that it will be pocketable with a kinda blackberry/pocket chip
sorta form factor. I plan to have a battery and maybe a small secondary lcd to display
battery percentage. This device is planned to run in BASH only, no GUI partially for
increased performance and partally to challenge myself to get better at command line.
Some insparation for this decision came from several Brian Lunduke videos on YouTube.
If you haven't heard of him, check him out, he is pretty cool.

Please note I am shamelessly stealing code and instructions from anywhere I can find it
when I think it will help with the project. I will make a note of anything that I have
done myself that is unique to this project and or made by me. I don't suspect that there
will be a lot of that as I feel like this project will moslty be an excersise in gathering
resources from a plethora of places into one where I can put it all together.

If anyone knows of someting that might help with this project please let me know.

Also if you are following this, please be patient with me, this project is waiting on a lot
of things such as info, money, and time. As I am a student, I am low on several of these
Ill make progress as fast as I make progress.

I am putting this under the MIT License, obviously this will not change the status of anything I do not
own in this project, but the actuall aggragate and anything I make are under MIT
feel free to use or share this info. Also I am really new to projects and 
licensing in general so if i have done the license wrong, plese let me know.
