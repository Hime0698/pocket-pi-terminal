#include "U8glib.h"
int analogInput = 0;
int Percent ;  //We want it as a whole number
float Voltageout = 0.0;
float Voltage = 0.0; //We want it with decimal places
float R1 = 97000.0; //resistance 
float R2 = 10000.0;  //resistance 
int value = 0;

U8GLIB_SH1106_128X64 u8g(10, 9, 12, 11, 13); // pinout, see page

void draw(void) 
{
  u8g.setFont(u8g_font_helvB14r); // font
  u8g.drawStr(19, 16, "BATTERY"); 
  u8g.setPrintPos(7,55);
  u8g.setFont(u8g_font_helvB18r);
  u8g.println(Percent);           //Prints Percent
  u8g.println("%");
  u8g.setPrintPos(63,55);
  u8g.setFont(u8g_font_helvB18r); //A slightly larger font
   u8g.println(Voltage);             //Prints the voltage
  u8g.println("V");
  u8g.drawRFrame(0, 23, 128, 1, 0); 
}
void setup(){
   pinMode(analogInput, INPUT);
}
void loop(){
   value = analogRead(analogInput);
   Voltageout = (value * 5.0) / 1024.0; 
   Voltage = Voltageout / (R2/(R1+R2)); 
   if (Voltage<1) {
   Voltage=0.0;  // get  rid of unwanted readings
   }
   if (Voltage>5.00) {  // if Reading are too large
    Voltage= Voltage/10;
   Percent = ((Voltage-3)/(4.2-3))*100; //votage divided by max cell votage times 100 = the cells current percentage
} 
  u8g.firstPage();  
  do 
    {
     draw();      
    }
  while( u8g.nextPage() );
delay(500);
}
